# Studio City
![Studio City](https://gitlab.com/perelax/src_imgs/-/raw/main/Studio%20City/studio_city_theme.png)*Studio City with Material Icons theming*

## Description
Dark blue theme with modified Darcula syntax coloring for a crisp look and feel.

## Installation
To install theme:

 - Select 'File' --> 'Settings'.
 - Open the 'Plugins' menu and select the 'more' icon.
 - Select 'Install Plugin from Disk' and locate the downloaded theme jar file.  
 
![Installation.](https://gitlab.com/perelax/src_imgs/-/raw/main/Studio%20City/install_theme.png)*Installation*

## Support
  
Feel like saying thank you?  

[![Buy Me A Coffee](https://cdn.buymeacoffee.com/buttons/default-orange.png){width=120}](https://www.buymeacoffee.com/perelax)

## Authors
Perelax

## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

See <https://www.gnu.org/licenses/> for a copy of the GNU General Public License.
